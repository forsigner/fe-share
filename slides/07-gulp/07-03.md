# Gulp usage

<br/>

```js

gulp.task('default', ['build']);

gulp.task('build', ['browserify', 'styles', 'html', 'copy']);

gulp.task('styles', function() {
  return gulp.src(srcPath + '/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist + '/css'));
});

gulp.task('copy', function() {
  gulp.src(srcPath + '/fonts/*')
    .pipe(gulp.dest(dist + '/fonts'));
});

gulp.task('browserify', ['templates'], function() {
  return browserify(srcPath + '/js/app.js')
    .bundle()
    .pipe(source('app.js'))
    .pipe(ngAnnotate({add: true}))
    .pipe(gulp.dest(dist + '/js'));
});

gulp.task('html', function() {
  var assets = useref.assets();
  return gulp.src(srcPath + '/*.html')
    .pipe(assets)
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulp.dest(dist));
});
```
