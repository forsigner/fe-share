# Overview

[Node.js](https://nodejs.org/en/)

[npm](https://www.npmjs.com/)

[Bower](http://bower.io/)

[Gulp](http://gulpjs.com/)

[Mock](https://github.com/forsigner/mocer)

[AngularJS](https://angularjs.org/)
