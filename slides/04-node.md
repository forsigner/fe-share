# Node.js

- built on Chrome's V8 JavaScript engine.
- event-driven, non-blocking I/O model
- lightweight and efficient

<br/>

**Who use**?

PayPal, LinkedIn, Uber, eBay, Medium, New York Times, Taobao...
