### Setup a Project

``` bash
$ brew i node # instal node (Mac)
$ npm i -g gulp # install gulp globally
$ npm i -g bower # install bower globally

$ git clone https://github.com/xgsdk2/xgsdk2-portal.git
$ cd xgsdk2-portal
$ npm install
$ bower install

$ gulp # start server
$ gulp deploy
```
